"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const api_service_1 = tslib_1.__importDefault(require("./api.service"));
class AuthService {
    constructor() {
        this.request = api_service_1.default;
    }
    authenticate(data) {
        return this.request.post('/server/authenticate', data);
    }
}
const Auth = new AuthService();
exports.default = Auth;
