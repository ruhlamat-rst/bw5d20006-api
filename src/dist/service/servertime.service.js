"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const api_service_1 = tslib_1.__importDefault(require("./api.service"));
class ServertimeService {
    constructor() {
        this.request = api_service_1.default;
    }
    getServerTime(data) {
        return this.request.get('/server/servertime', data);
    }
}
const servertime = new ServertimeService();
exports.default = servertime;
