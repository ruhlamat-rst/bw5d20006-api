"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthJS = void 0;
const tslib_1 = require("tslib");
const api_service_1 = tslib_1.__importDefault(require("../service/api.service"));
const auth_service_1 = tslib_1.__importDefault(require("../service/auth.service"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const events_1 = tslib_1.__importDefault(require("events"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const Utility = tslib_1.__importStar(require("./utility"));
const emitter = new events_1.default.EventEmitter();
function auth(config = config_json_1.default, utility = Utility) {
    const credential = config.credential;
    const defaults = config.defaults;
    const requestTimeout = defaults.requesttimeout || 40;
    const retryServer = defaults.retryServerTimer || 10;
    api_service_1.default.setTimeout(requestTimeout);
    api_service_1.default.setDefaultHeader(config.loginType);
    const log = bunyan_1.default.createLogger({
        name: `auth`,
        level: config.logger.loglevel || 20,
    });
    return {
        name: `authentication`,
        onStartup: false,
        authResponse: {},
        getAuthentication() {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                try {
                    const payload = {
                        identifier: credential.identifier,
                        password: credential.password,
                    };
                    const response = yield auth_service_1.default.authenticate(payload);
                    const { status, data } = response;
                    if (status === utility.HTTP_STATUS_CODE.SUCCESS && data) {
                        api_service_1.default.setHeader(data.sessionId);
                        if (!this.onStartup) {
                            this.onStartup = true;
                            emitter.emit('init');
                        }
                    }
                    else {
                        log.error(`Error in authentication to server ${JSON.stringify(response.data)}`);
                        yield utility.setTimer(retryServer);
                        yield this.getAuthentication();
                    }
                }
                catch (ex) {
                    log.error(`Exception in authentication ${ex}`);
                    yield utility.setTimer(retryServer);
                    yield this.getAuthentication();
                }
            });
        },
    };
}
exports.AuthJS = {
    emitter,
    auth,
};
