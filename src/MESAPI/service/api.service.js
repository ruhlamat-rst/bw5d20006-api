"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const axios_1 = tslib_1.__importDefault(require("axios"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
class ApiService {
    constructor() {
        this.instance = axios_1.default.create({
            baseURL: `${config_json_1.default.server.protocol}://${config_json_1.default.server.host}:${config_json_1.default.server.port}`,
        });
    }
    setTimeout(timeout) {
        this.instance.defaults.timeout = (timeout || 5) * 1000;
    }
    setDefaultHeader(loginType) {
        this.instance.defaults.headers.common.loginType = loginType;
    }
    setHeader(session) {
        this.instance.defaults.headers.common.sessionId = session;
        this.instance.defaults.headers.cookie = `JSESSIONID=${session}; sessionId=${session}`;
    }
    removeHeader() {
        this.instance.defaults.headers.common = {};
    }
    request(method, url, data = {}, config = {}) {
        return this.instance.request(Object.assign({ method,
            url,
            data }, config));
    }
    get(url, config = {}) {
        return this.request('GET', url, {}, config);
    }
    post(url, data, config = {}) {
        return this.request('POST', url, data, config);
    }
    put(url, data, config = {}) {
        return this.request('PUT', url, data, config);
    }
    patch(url, data, config = {}) {
        return this.request('PATCH', url, data, config);
    }
    delete(url, config = {}) {
        return this.request('DELETE', url, {}, config);
    }
}
const Api = new ApiService();
exports.default = Api;
