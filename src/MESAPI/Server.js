"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
const tslib_1 = require("tslib");
const morgan_1 = tslib_1.__importDefault(require("morgan"));
const helmet_1 = tslib_1.__importDefault(require("helmet"));
const cors_1 = tslib_1.__importDefault(require("cors"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const config_json_1 = tslib_1.__importDefault(require("./config/config.json"));
const express_1 = tslib_1.__importDefault(require("express"));
require("express-async-errors");
const index_1 = tslib_1.__importDefault(require("./routes/index"));
const app = express_1.default();
exports.app = app;
const log = bunyan_1.default.createLogger({
    name: `index`,
    level: config_json_1.default.logger.loglevel,
});
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
app.use(cors_1.default());
if (process.env.NODE_ENV === 'development') {
    app.use(morgan_1.default('dev'));
}
if (process.env.NODE_ENV === 'production') {
    app.use(helmet_1.default());
}
app.use((err, req, res, next) => {
    log.error(err);
    res.sendStatus(500).json(err);
});
app.use('/api', index_1.default);
