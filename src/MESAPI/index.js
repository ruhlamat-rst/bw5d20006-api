"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
require("./LoadEnv");
const config_json_1 = tslib_1.__importDefault(require("./config/config.json"));
const UTILITY = tslib_1.__importStar(require("./utility/utility"));
const auth_1 = require("./utility/auth");
const Server_1 = require("./Server");
const servertime_1 = require("./utility/servertime");
const auth = auth_1.AuthJS.auth(config_json_1.default, UTILITY);
const servertime = servertime_1.ServerTimeJS.servertime(config_json_1.default, UTILITY);
const port = process.env.PORT || 3000;
const log = bunyan_1.default.createLogger({
    name: `index`,
    level: config_json_1.default.logger.loglevel,
});
UTILITY.emitter.on('sessionExpired', () => {
    log.info(`Session Expired trying to reAuthenticate`);
    auth.getAuthentication();
});
auth_1.AuthJS.emitter.on('init', () => {
    log.error(`Authentication done successfully`);
    servertime.getServerTime();
    Server_1.app.listen(port, () => {
        log.error(`API server started on port ${port}`);
    });
});
auth.getAuthentication();
