import { Router } from 'express';
import Controller from '../controller/controller';

const router = Router();
// Add sub-routes
router.post('/postOrder', Controller.commitOrder);
router.post('/postComponentlifealarm', Controller.commitComponentLifeAlarm);

export default router;
