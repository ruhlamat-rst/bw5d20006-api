'use strict';

import servertimeService from '../service/servertime.service';
import bunyan from 'bunyan';
import Config from '../config/config.json';
import * as Utility from './utility';
function servertime(config = Config, utility = Utility) {
  const log = bunyan.createLogger({
    name: 'ServerTime',
    level: config.logger.loglevel,
  });
  let retryServerTimer = 10;
  return {
    /**
     * This method all the recipes from recipedetails element
     */
    async getServerTime() {
      try {
        const response = await servertimeService.getServerTime({});
        if (response.data && response.data.results) {
          // server time received
        } else {
          utility.checkSessionExpired(response.data);
        }
      } catch (ex) {
        log.error(`Exception to fetch servertime ${ex}`);
      }
      await utility.setTimer(retryServerTimer);
      this.getServerTime();
    },
  };
}
export const ServerTimeJS = {
  servertime,
};
