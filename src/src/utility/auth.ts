import apiService from '../service/api.service';
import authService from '../service/auth.service';
import bunyan from 'bunyan';
import EventEmitter from 'events';
import Config from '../config/config.json';
import * as Utility from './utility';
const emitter = new EventEmitter.EventEmitter();
function auth(config = Config, utility = Utility) {
  const credential = config.credential;
  const defaults = config.defaults;
  const requestTimeout = defaults.requesttimeout || 40;
  const retryServer = defaults.retryServerTimer || 10;
  apiService.setTimeout(requestTimeout);
  // set loginType in axios header
  apiService.setDefaultHeader(config.loginType);
  const log = bunyan.createLogger({
    name: `auth`,
    level: config.logger.loglevel || 20,
  });
  return {
    name: `authentication`,
    onStartup: false,
    authResponse: {},
    /**
     * This method logged in to server using credentials configured in config file
     */
    async getAuthentication() {
      try {
        const payload = {
          identifier: credential.identifier,
          password: credential.password,
        };
        const response = await authService.authenticate(payload);
        const { status, data } = response;
        if (status === utility.HTTP_STATUS_CODE.SUCCESS && data) {
          apiService.setHeader(data.sessionId);
          if (!this.onStartup) {
            this.onStartup = true;
            emitter.emit('init');
          }
        } else {
          // retry for authentication
          log.error(
            `Error in authentication to server ${JSON.stringify(response.data)}`
          );
          await utility.setTimer(retryServer);
          await this.getAuthentication();
        }
      } catch (ex) {
        log.error(`Exception in authentication ${ex}`);
        await utility.setTimer(retryServer);
        await this.getAuthentication();
      }
    },
  };
}
export const AuthJS = {
  emitter,
  auth,
};
