import morgan from 'morgan';
import path from 'path';
import helmet from 'helmet';
import cors from 'cors';
import bunyan from 'bunyan';
import CONFIG from './config/config.json';
import express, { Request, Response, NextFunction } from 'express';
import 'express-async-errors';
import BaseRouter from './routes/index';

const app = express();

const log = bunyan.createLogger({
  name: `index`,
  level: CONFIG.logger.loglevel,
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}
if (process.env.NODE_ENV === 'production') {
  app.use(helmet());
}

//handle error
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  log.error(err);
  res.sendStatus(500).json(err);
});

// routers
app.use('/api', BaseRouter);

export { app };
