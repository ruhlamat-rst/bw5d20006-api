import { Request, Response, NextFunction } from 'express';
import elementService from '../service/element.service';
import CONFIG from '../config/config.json';
import * as Utility from '../utility/utility';
import momnet from 'moment';
import bunyan from 'bunyan';
import { resolve } from 'path';
import { reject } from 'lodash';

const log = bunyan.createLogger({
  name: `controller`,
  level: CONFIG.logger.loglevel,
});
let retryServerTimer = CONFIG.defaults.retryServerTimer; // in seconds

interface UpdatePayload {
  ordername: string;
  orderstatus: 'Running' | 'Interrupted' | 'Completed';
  assetid?: number;
}

class Controller {
  constructor() {
    this.commitOrder = this.commitOrder.bind(this);
    this.writeorderproduct = this.writeorderproduct.bind(this);
    this.writeorderroadmap = this.writeorderroadmap.bind(this);
  }
  /**
   * MES post order to SWX
   * 1: new order start => update running order to completed, start new older
   * 2: running order hold => update running order to hold,
   * 3: running order unhold => update running order to completed, start new older
   */
  async commitOrder(req: Request, res: Response, next: NextFunction) {
    const {
      ProductionLineNo,
      Quantity,
      WipOrderNo,
      WipOrderType,
      ProductNo,
      ProductId,
      RecipeName,
      WipOrderStatus,
      ReworkStartEquipment,
    } = req.body;
    try {
      if (WipOrderStatus === 1) {
        // new order
        // STEP 1: Completed running order
        let completedpayload: UpdatePayload = {
          ordername: '',
          orderstatus: 'Completed',
        };
        const completedresponse = await this.updateOrderStatus(
          completedpayload
        );
        if (completedresponse.status == 200) {
          // SETP 2: Add new running order or start interrupt
          // First : check if exists
          let orderquery = `query=ordername=="${WipOrderNo}"`;
          const orderresponse = await elementService.getElementRecords(
            CONFIG.elements.order || 'order',
            orderquery
          );
          if (orderresponse.status == 200) {
            if (
              orderresponse.data &&
              orderresponse.data.results &&
              orderresponse.data.results.length > 0
            ) {
              // update order status to interrupted
              let update: UpdatePayload = {
                orderstatus: 'Running',
                ordername: WipOrderNo,
              };
              let updateresponse = await this.updateOrderStatus(update);
              if (updateresponse.status === 200) {
                log.error(
                  `Record saved successfully in ShopWorx for new order ${JSON.stringify(
                    updateresponse.data
                  )}`
                );
                res.status(200);
                res.json({
                  msg: `Record saved successfully in ShopWorx for new order`,
                });
              } else {
                log.error(
                  `Error in writing new order data in ShopWorx ${JSON.stringify(
                    updateresponse.data
                  )}`
                );
                Utility.checkSessionExpired(updateresponse.data);
                res.status(400);
                res.json({
                  msg: `Error in writing new order data in ShopWorx`,
                });
              }
            } else {
              // Second: get recipe
              let roadmapquery = ``;
              const roadmapresponse = await elementService.getElementRecords(
                CONFIG.elements.roadmaplist || 'roadmaplist',
                roadmapquery
              );
              if (roadmapresponse.status == 200) {
                let roadmapid = '';
                let roadmapname = '';
                let roadmaptype = '';
                if (
                  roadmapresponse.data &&
                  roadmapresponse.data.results &&
                  roadmapresponse.data.results.length > 0
                ) {
                  roadmapid = roadmapresponse.data.results[0].id;
                  roadmapname = roadmapresponse.data.results[0].name;
                  roadmaptype = roadmapresponse.data.results[0].roadmaptype;
                }
                let producttypequery = `query=productname=="${RecipeName}"`;
                const producttyperesponse = await elementService.getElementRecords(
                  'part',
                  producttypequery
                );
                if (producttyperesponse.status == 200) {
                  if (
                    producttyperesponse.data &&
                    producttyperesponse.data.results &&
                    producttyperesponse.data.results.length > 0
                  ) {
                    const bomid = producttyperesponse.data.results[0].bomid;
                    const productnumber =
                      producttyperesponse.data.results[0].productnumber;
                    const productname = RecipeName;

                    let payload = {
                      assetid: CONFIG.assetid || 4,
                      lineid: 1,
                      linename: ProductionLineNo,
                      ordercreatedtime: momnet().valueOf(),
                      ordername: WipOrderNo,
                      ordernumber: WipOrderNo,
                      orderstatus: 'New',
                      ordertype:
                        Number(WipOrderType || 1) == 1 ? 'Work' : 'Rework',
                      productid: productnumber,
                      productname: productname,
                      roadmapid: roadmapid,
                      roadmapname: roadmapname,
                      roadmaptype: roadmaptype,
                      scheduledstart: momnet().valueOf(),
                      targetcount: Quantity,
                      reworktargetstation: ReworkStartEquipment,
                      mesrecipe: ProductNo,
                      source: 2,
                      bomid: bomid,
                    };
                    const response = await elementService.createElementRecord(
                      CONFIG.elements.order || 'order',
                      payload
                    );
                    if (response.status === 200) {
                      log.error(
                        `Record saved successfully in ShopWorx for new order ${JSON.stringify(
                          response.data
                        )}`
                      );
                      let orderquery = `query=ordername=="${WipOrderNo}"`;
                      orderquery += `&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`;
                      const orderresponse = await elementService.getElementRecords(
                        CONFIG.elements.order || 'order',
                        orderquery
                      );
                      if (orderresponse.status == 200) {
                        let order = orderresponse.data.results[0];
                        let ordernumber = order.ordernumber;
                        const substationresponse = await elementService.getElementRecords(
                          CONFIG.elements.substation || 'substation',
                          ''
                        );
                        if (substationresponse.status == 200) {
                          if (
                            substationresponse.data &&
                            substationresponse.data.results &&
                            substationresponse.data.results.length > 0
                          ) {
                            // add order product
                            let orderproductpromises = substationresponse.data.results.map(
                              (item: any) => {
                                return new Promise((resolve) => {
                                  let payload = {
                                    lineid: item.lineid,
                                    orderid: ordernumber,
                                    ordername: WipOrderNo,
                                    productid: productnumber,
                                    sublineid: item.sublineid,
                                    substationid: item.id,
                                  };
                                  this.writeorderproduct(payload, resolve);
                                });
                              }
                            );
                            // add order roadmap
                            let orderroadmappromises = substationresponse.data.results.map(
                              (item: any) => {
                                return new Promise((resolve) => {
                                  this.writeorderroadmap(
                                    ordernumber,
                                    roadmapid,
                                    item.id,
                                    resolve
                                  );
                                });
                              }
                            );
                            Promise.all([
                              ...orderproductpromises,
                              ...orderroadmappromises,
                            ]).then(async () => {
                              // update order to run
                              let startorder: UpdatePayload = {
                                ordername: WipOrderNo,
                                orderstatus: 'Running',
                              };
                              const updateorderresponse = await this.updateOrderStatus(
                                startorder
                              );
                              if (updateorderresponse.status == 200) {
                                res.status(200);
                                res.json({
                                  msg: `Record saved successfully in ShopWorx for new order`,
                                });
                              } else {
                                Utility.checkSessionExpired(
                                  updateorderresponse.data
                                );
                                res.status(400);
                                res.json({
                                  msg: `Error in writing new order data in ShopWorx`,
                                });
                                await this.updateOrderStatus(startorder);
                              }
                            });
                          }
                        } else {
                          Utility.checkSessionExpired(substationresponse.data);
                          res.status(400);
                          res.json({
                            msg: `Error in writing new order data in ShopWorx`,
                          });
                        }
                      } else {
                        Utility.checkSessionExpired(orderresponse.data);
                        res.status(400);
                        res.json({
                          msg: `Error in writing new order data in ShopWorx`,
                        });
                      }
                    } else {
                      log.error(
                        `Error in writing new order data in ShopWorx ${JSON.stringify(
                          response.data
                        )}`
                      );
                      Utility.checkSessionExpired(response.data);
                      res.status(400);
                      res.json({
                        msg: `Error in writing new order data in ShopWorx`,
                      });
                    }
                  } else {
                  }
                } else {
                }
              } else {
                Utility.checkSessionExpired(roadmapresponse.data);
                res.status(400);
                res.json({
                  msg: `Error in writing new order data in ShopWorx`,
                });
              }
            }
          } else {
            Utility.checkSessionExpired(orderresponse.data);
            res.status(400);
            res.json({
              msg: `Error in writing new order data in ShopWorx`,
            });
          }
        } else {
          Utility.checkSessionExpired(completedresponse.data);
          res.status(400);
          res.json({
            msg: `Error in writing new order data in ShopWorx `,
          });
        }
      } else {
        // update order status to interrupted
        let update: UpdatePayload = {
          orderstatus: 'Interrupted',
          ordername: WipOrderNo,
        };
        let updateresponse = await this.updateOrderStatus(update);
        if (updateresponse.status === 200) {
          log.error(
            `Record saved successfully in ShopWorx for new order ${JSON.stringify(
              updateresponse.data
            )}`
          );
          res.status(200);
          res.json({
            msg: `Record saved successfully in ShopWorx for new order`,
          });
        } else {
          log.error(
            `Error in writing new order data in ShopWorx ${JSON.stringify(
              updateresponse.data
            )}`
          );
          Utility.checkSessionExpired(updateresponse.data);
          res.status(400);
          res.json({
            msg: `Error in writing new order data in ShopWorx`,
          });
        }
      }
    } catch (error) {
      next(error);
    }
  }
  async commitComponentLifeAlarm(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const {
      ProductionLineNo,
      Equipment,
      Reasoncode,
      LifeType,
      CountValue,
    } = req.body;
    // TODO: post alarm to shopworx
    const eqArr = (Equipment as string).split('_');
    const equipmentname = `${eqArr[eqArr.length - 2]}_${
      eqArr[eqArr.length - 1]
    }`;
    const elementName =
      CONFIG.elements.componentlifealarm || 'componentlifealarm';
    try {
      let substationquery = `query=name=="${equipmentname}"`;
      const substationresponse = await elementService.getElementRecords(
        CONFIG.elements.substation || 'substation',
        substationquery
      );
      if (substationresponse.status === 200) {
        let substationid = '';
        if (
          substationresponse.data &&
          substationresponse.data.results &&
          substationresponse.data.results.length > 0
        ) {
          substationid = substationresponse.data.results[0].id;
        }
        let payload = {
          assetid: 4,
          lineid: 1,
          linename: ProductionLineNo,
          substationname: equipmentname,
          substationid: substationid,
          reasoncode: Reasoncode,
          lifetype: LifeType || '',
          count: CountValue || 0,
        };
        const response = await elementService.createElementRecord(
          elementName,
          payload
        );
        if (response.status === 200) {
          log.error(
            `Record saved successfully in ShopWorx for componentlifealarm ${JSON.stringify(
              response.data
            )}`
          );
          res.status(200);
          res.json({
            msg: `Record saved successfully in ShopWorx for componentlifealarm ${JSON.stringify(
              response.data
            )}`,
          });
        } else {
          log.error(
            `Error in writing componentlifealarm data in ShopWorx ${JSON.stringify(
              response.data
            )}`
          );
          Utility.checkSessionExpired(response.data);
          res.status(400);
          res.json({
            msg: `Error in writing componentlifealarm data in ShopWorx`,
          });
        }
      } else {
        log.error(
          `Error in writing componentlifealarm data in ShopWorx ${JSON.stringify(
            substationresponse.data
          )}`
        );
        Utility.checkSessionExpired(substationresponse.data);
        res.status(400);
        res.json({
          msg: `Error in writing componentlifealarm data in ShopWorx`,
        });
      }
    } catch (error) {
      next(error);
    }
  }
  updateOrderStatus(payload: UpdatePayload) {
    const elementName = CONFIG.elements.order || 'order';
    payload.assetid = CONFIG.assetid || 4;
    let query = '';
    if (payload.orderstatus === 'Completed') {
      query = `query=orderstatus=="Running"`;
    } else {
      query = `query=ordername=="${payload.ordername}"`;
    }
    query += `&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`;
    return elementService.updateElementRecordsByQuery(
      elementName,
      payload,
      query
    );
  }
  async writeorderproduct(payload: any, resolve?: any) {
    const elementName = 'orderproduct';
    payload.assetid = CONFIG.assetid || 4;
    const response = await elementService.createElementRecord(
      elementName,
      payload
    );
    if (response.status === 200) {
      log.error(
        `Record saved successfully in ShopWorx for orderroadmap ${JSON.stringify(
          response.data
        )}`
      );
      if (resolve) resolve();
    } else {
      if (resolve) resolve();
      log.error(
        `Error in writing orderroadmap data in ShopWorx ${JSON.stringify(
          response.data
        )}`
      );
      Utility.checkSessionExpired(response.data);
      this.writeorderproduct(payload);
    }
  }
  async writeorderroadmap(
    ordernumber: any,
    roadmapid: any,
    substationid: any,
    resolve?: any
  ) {
    // roadmap detail
    let roadmapdetailquery = `query=roadmapid=="${roadmapid}"`;
    roadmapdetailquery += `%26%26substationid=="${substationid}"`;
    const roadmapdetailresponse = await elementService.getElementRecords(
      CONFIG.elements.roadmapdetails || 'roadmapdetail',
      roadmapdetailquery
    );
    if (roadmapdetailresponse.status == 200) {
      if (
        roadmapdetailresponse.data &&
        roadmapdetailresponse.data.results &&
        roadmapdetailresponse.data.results.length > 0
      ) {
        const roadmapdetail = roadmapdetailresponse.data.results[0];
        const elementName = 'orderroadmap';
        let payload = {
          amtpresubstation: roadmapdetail.amtpresubstation,
          assetid: CONFIG.assetid || 4,
          lineid: 1,
          orderid: ordernumber,
          prestationid: roadmapdetail.prestationid,
          prestationname: roadmapdetail.prestationname,
          presubstationid: roadmapdetail.presubstationid,
          presubstationname: roadmapdetail.presubstationname,
          roadmapid: roadmapdetail.roadmapid,
          sublineid: roadmapdetail.sublineid,
          sublinename: roadmapdetail.sublinename,
          substationid: roadmapdetail.substationid,
          substationname: roadmapdetail.substationname,
        };
        const response = await elementService.createElementRecord(
          elementName,
          payload
        );
        if (response.status === 200) {
          log.error(
            `Record saved successfully in ShopWorx for orderroadmap ${JSON.stringify(
              response.data
            )}`
          );
          if (resolve) resolve();
        } else {
          if (resolve) resolve();
          log.error(
            `Error in writing orderroadmap data in ShopWorx ${JSON.stringify(
              response.data
            )}`
          );
          Utility.checkSessionExpired(response.data);
          this.writeorderroadmap(ordernumber, roadmapid, substationid);
        }
      } else {
        resolve();
      }
    } else {
      log.error(
        `Error in writing orderroadmap data in ShopWorx ${JSON.stringify(
          roadmapdetailresponse.data
        )}`
      );
      Utility.checkSessionExpired(roadmapdetailresponse.data);
      this.writeorderroadmap(ordernumber, roadmapid, substationid, resolve);
    }
  }
}

export default new Controller();
