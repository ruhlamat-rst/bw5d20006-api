import axios, { AxiosInstance, Method } from 'axios';
import config from '../config/config.json';

class ApiService {
  instance: AxiosInstance;
  constructor() {
    this.instance = axios.create({
      baseURL: `${config.server.protocol}://${config.server.host}:${config.server.port}`,
    });
  }
  setTimeout(timeout: number) {
    this.instance.defaults.timeout = (timeout || 5) * 1000;
  }
  setDefaultHeader(loginType: string) {
    this.instance.defaults.headers.common.loginType = loginType;
  }
  setHeader(session: string) {
    this.instance.defaults.headers.common.sessionId = session;
    this.instance.defaults.headers.cookie = `JSESSIONID=${session}; sessionId=${session}`;
  }

  removeHeader() {
    this.instance.defaults.headers.common = {};
  }

  request(method: Method, url: string, data = {}, config = {}) {
    return this.instance.request({
      method,
      url,
      data,
      ...config,
    });
  }

  get(url: string, config = {}) {
    return this.request('GET', url, {}, config);
  }

  post(url: string, data: any, config = {}) {
    return this.request('POST', url, data, config);
  }

  put(url: string, data: any, config = {}) {
    return this.request('PUT', url, data, config);
  }

  patch(url: string, data: any, config = {}) {
    return this.request('PATCH', url, data, config);
  }

  delete(url: string, config = {}) {
    return this.request('DELETE', url, {}, config);
  }
}
const Api = new ApiService();
export default Api;
