import ApiService from './api.service';

class ElementService {
  request: typeof ApiService;
  constructor() {
    this.request = ApiService;
  }

  createElement(data: any) {
    return this.request.post('/server/elements', data);
  }

  getElement(elementName: string) {
    return this.request.get(`/server/elements/${elementName}`);
  }

  getElementRecords(elementName: string, query: string) {
    let url = `/server/elements/${elementName}/records`;
    if (query) {
      url += `?${query}`;
    }
    return this.request.get(`${url}`);
  }

  getElementRecordByPost(elementName: string, data: any, query: string) {
    let url = `/server/elements/${elementName}/records`;
    if (query) {
      url += `?${query}`;
    }
    return this.request.post(`${url}`, data);
  }

  createElementRecord(elementName: string, data = {}) {
    return this.request.post(`/server/elements/${elementName}/records`, data);
  }

  createElementRecordV2(elementName: string, data = {}) {
    return this.request.post(
      `/server/elements/${elementName}/records/v2`,
      data
    );
  }

  createElementMultipleRecords(elementName: string, data: any) {
    return this.request.post(
      `/server/elements/${elementName}/createbulkrecords`,
      data
    );
  }

  createElementMultipleRecordsV2(elementName: string, data: any) {
    return this.request.post(
      `/server/elements/${elementName}/createbulkrecords/v2`,
      data
    );
  }

  updateElementMultipleRecords(elementName: string, data: any) {
    return this.request.put(
      `/server/elements/${elementName}/updatemultiplerecords`,
      data
    );
  }

  updateElementRecordById(elementName: string, data: any, id: string) {
    return this.request.put(
      `/server/elements/${elementName}/records/${id}`,
      data
    );
  }

  getMultipleElementsQueryRecords(data: string) {
    return this.request.post(`/server/elements/records`, data);
  }

  updateElementRecordsByQuery(elementName: string, data: any, query: string) {
    let url = `/server/elements/${elementName}/records`;
    if (query) {
      url += `?${query}`;
    }
    return this.request.put(url, data);
  }

  upsertElementRecordsByQuery(elementName: string, data: any, query: string) {
    let url = `/server/elements/${elementName}/records/createorupdate`;
    if (query) {
      url += `?${query}`;
    }
    return this.request.put(url, data);
  }
}

const Element = new ElementService();
export default Element;
