import bunyan from 'bunyan';
import './LoadEnv';
import CONFIG from './config/config.json';
import * as UTILITY from './utility/utility';
import { AuthJS } from './utility/auth';
import { app } from './Server';
import { ServerTimeJS } from './utility/servertime';

const auth = AuthJS.auth(CONFIG, UTILITY);
const servertime = ServerTimeJS.servertime(CONFIG, UTILITY);

const port = process.env.PORT || 3000;
const log = bunyan.createLogger({
  name: `index`,
  level: CONFIG.logger.loglevel,
});

/**
 * Listen sessionExpired Event and authenticate again
 */
UTILITY.emitter.on('sessionExpired', () => {
  log.info(`Session Expired trying to reAuthenticate`);
  auth.getAuthentication();
});

/**
 * Authenticate MESAPI before initialization
 */
AuthJS.emitter.on('init', () => {
  log.error(`Authentication done successfully`);
  servertime.getServerTime();
  app.listen(port, () => {
    log.error(`API server started on port ${port}`);
  });
});

auth.getAuthentication();
