"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const element_service_1 = tslib_1.__importDefault(require("../service/element.service"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const Utility = tslib_1.__importStar(require("../utility/utility"));
const moment_1 = tslib_1.__importDefault(require("moment"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const log = bunyan_1.default.createLogger({
    name: `controller`,
    level: config_json_1.default.logger.loglevel,
});
let retryServerTimer = config_json_1.default.defaults.retryServerTimer;
class Controller {
    constructor() { 
	this.commitOrder = this.commitOrder.bind(this);
    }
    commitOrder(req, res, next) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
debugger;
            const { ProductionLineNo, Quantity, WipOrderNo, WipOrderType, ProductNo, ProductId, RecipeName, WipOrderStatus, ReworkStartEquipment, } = req.body;
            try {
                if (WipOrderStatus === 1) {
                    let completedpayload = {
                        ordername: '',
                        orderstatus: 'Completed',
                    };
                    const completedresponse = yield this.updateOrderStatus(completedpayload);
                    if (completedresponse.status == 200) {
                        let orderquery = `query=ordername=="${WipOrderNo}"`;
                        const orderresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.order || 'order', orderquery);
                        if (orderresponse.status == 200) {
                            if (orderresponse.data &&
                                orderresponse.data.results &&
                                orderresponse.data.results.length > 0) {
                                let update = {
                                    orderstatus: 'Running',
                                    ordername: WipOrderNo,
                                };
                                let updateresponse = yield this.updateOrderStatus(update);
                                if (updateresponse.status === 200) {
                                    log.error(`Record saved successfully in ShopWorx for new order ${JSON.stringify(updateresponse.data)}`);
                                    res.status(200);
                                    res.json({
                                        msg: `Record saved successfully in ShopWorx for new order`,
                                    });
                                }
                                else {
                                    log.error(`Error in writing new order data in ShopWorx ${JSON.stringify(updateresponse.data)}`);
                                    Utility.checkSessionExpired(updateresponse.data);
                                    res.status(400);
                                    res.json({
                                        msg: `Error in writing new order data in ShopWorx`,
                                    });
                                }
                            }
                            else {
                                let roadmapquery = ``;
                                const roadmapresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.roadmaplist || 'roadmaplist', roadmapquery);
                                if (roadmapresponse.status == 200) {
                                    let roadmapid = '';
                                    let roadmapname = '';
                                    let roadmaptype = '';
                                    if (roadmapresponse.data &&
                                        roadmapresponse.data.results &&
                                        roadmapresponse.data.results.length > 0) {
                                        roadmapid = roadmapresponse.data.results[0].id;
                                        roadmapname = roadmapresponse.data.results[0].name;
                                        roadmaptype = roadmapresponse.data.results[0].roadmaptype;
                                    }
                                    let payload = {
                                        assetid: config_json_1.default.assetid || 4,
                                        lineid: 1,
                                        linename: ProductionLineNo,
                                        ordercreatedtime: moment_1.default().valueOf(),
                                        ordername: WipOrderNo,
                                        ordernumber: WipOrderNo,
                                        orderstatus: 'Running',
                                        ordertype: WipOrderType==1?"Work":"Rework",
                                        productid: ProductId || '',
                                        productname: ProductNo || '',
                                        roadmapid: roadmapid,
                                        roadmapname: roadmapname,
                                        roadmaptype: roadmaptype,
                                        scheduledstart: moment_1.default().valueOf(),
                                        targetcount: Quantity,
                                        reworktargetstation: ReworkStartEquipment || '',
                                        mesrecipe: RecipeName || '',
                                        source: 2,
                                    };
                                    const response = yield element_service_1.default.createElementRecord(config_json_1.default.elements.order || 'order', payload);
                                    if (response.status === 200) {
                                        log.error(`Record saved successfully in ShopWorx for new order ${JSON.stringify(response.data)}`);
                                        res.status(200);
                                        res.json({
                                            msg: `Record saved successfully in ShopWorx for new order`,
                                        });
                                    }
                                    else {
                                        log.error(`Error in writing new order data in ShopWorx ${JSON.stringify(response.data)}`);
                                        Utility.checkSessionExpired(response.data);
                                        res.status(400);
                                        res.json({
                                            msg: `Error in writing new order data in ShopWorx`,
                                        });
                                    }
                                }
                                else {
                                    Utility.checkSessionExpired(roadmapresponse.data);
                                    res.status(400);
                                    res.json({
                                        msg: `Error in writing new order data in ShopWorx`,
                                    });
                                }
                            }
                        }
                        else {
                            Utility.checkSessionExpired(orderresponse.data);
                            res.status(400);
                            res.json({
                                msg: `Error in writing new order data in ShopWorx`,
                            });
                        }
                    }
                    else {
                        Utility.checkSessionExpired(completedresponse.data);
                        res.status(400);
                        res.json({
                            msg: `Error in writing new order data in ShopWorx `,
                        });
                    }
                }
                else {
                    let update = {
                        orderstatus: 'Interrupted',
                        ordername: WipOrderNo,
                    };
                    let updateresponse = yield this.updateOrderStatus(update);
                    if (updateresponse.status === 200) {
                        log.error(`Record saved successfully in ShopWorx for new order ${JSON.stringify(updateresponse.data)}`);
                        res.status(200);
                        res.json({
                            msg: `Record saved successfully in ShopWorx for new order`,
                        });
                    }
                    else {
                        log.error(`Error in writing new order data in ShopWorx ${JSON.stringify(updateresponse.data)}`);
                        Utility.checkSessionExpired(updateresponse.data);
                        res.status(400);
                        res.json({
                            msg: `Error in writing new order data in ShopWorx`,
                        });
                    }
                }
            }
            catch (error) {
                next(error);
            }
        });
    }
    commitComponentLifeAlarm(req, res, next) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { ProductionLineNo, Equipment, Reasoncode, LifeType, CountValue, } = req.body;
            const elementName = config_json_1.default.elements.componentlifealarm || 'componentlifealarm';
            try {
                let substationquery = `query=name=="${Equipment}"`;
                const substationresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.substation || 'substation', substationquery);
                if (substationresponse.status === 200) {
                    let substationid = '';
                    if (substationresponse.data &&
                        substationresponse.data.results &&
                        substationresponse.data.results.length > 0) {
                        substationid = substationresponse.data.results[0].id;
                    }
                    let payload = {
                        assetid: 4,
                        lineid: 1,
                        linename: 'Line 1',
                        substationname: Equipment,
                        substationid: substationid,
                        reasoncode: Reasoncode,
                        lifetype: LifeType || '',
                        count: CountValue || 0,
                    };
                    const response = yield element_service_1.default.createElementRecord(elementName, payload);
                    if (response.status === 200) {
                        log.error(`Record saved successfully in ShopWorx for componentlifealarm ${JSON.stringify(response.data)}`);
                        res.status(200);
                        res.json({
                            msg: `Record saved successfully in ShopWorx for componentlifealarm ${JSON.stringify(response.data)}`,
                        });
                    }
                    else {
                        log.error(`Error in writing componentlifealarm data in ShopWorx ${JSON.stringify(response.data)}`);
                        Utility.checkSessionExpired(response.data);
                        res.status(400);
                        res.json({
                            msg: `Error in writing componentlifealarm data in ShopWorx`,
                        });
                    }
                }
                else {
                    log.error(`Error in writing componentlifealarm data in ShopWorx ${JSON.stringify(substationresponse.data)}`);
                    Utility.checkSessionExpired(substationresponse.data);
                    res.status(400);
                    res.json({
                        msg: `Error in writing componentlifealarm data in ShopWorx`,
                    });
                }
            }
            catch (error) {
                next(error);
            }
        });
    }
    updateOrderStatus(payload) {
        const elementName = config_json_1.default.elements.order || 'order';
        payload.assetid = config_json_1.default.assetid || 4;
        let query = '';
        if (payload.orderstatus === 'Completed') {
            query = `query=orderstatus=="Running"`;
        }
        else {
            query = `query=ordername=="${payload.ordername}"`;
        }
        query += `&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`;
        return element_service_1.default.updateElementRecordsByQuery(elementName, payload, query);
    }
}
exports.default = new Controller();
