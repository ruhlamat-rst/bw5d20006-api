"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const element_service_1 = tslib_1.__importDefault(require("../service/element.service"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const Utility = tslib_1.__importStar(require("../utility/utility"));
const moment_1 = tslib_1.__importDefault(require("moment"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const log = bunyan_1.default.createLogger({
    name: `controller`,
    level: config_json_1.default.logger.loglevel,
});
let retryServerTimer = config_json_1.default.defaults.retryServerTimer;
class Controller {
    constructor() { 
	this.commitOrder = this.commitOrder.bind(this);
	this.writeorderproduct = this.writeorderproduct.bind(this);
        this.writeorderroadmap = this.writeorderroadmap.bind(this);
    }
    commitOrder(req, res, next) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            log.error(`new order payload ${JSON.stringify(req.body)}`);
            const { ProductionLineNo, Quantity, WipOrderNo, WipOrderType, ProductNo, ProductId, RecipeName, WipOrderStatus, ReworkStartEquipment, } = req.body;
            try {
                if (WipOrderStatus === 1) {
                    let completedpayload = {
                        ordername: '',
                        orderstatus: 'Completed',
                    };
                    const completedresponse = yield this.updateOrderStatus(completedpayload);
                    if (completedresponse.status == 200) {
                        let orderquery = `query=ordername=="${WipOrderNo}"`;
                        const orderresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.order || 'order', orderquery);
                        if (orderresponse.status == 200) {
                            if (orderresponse.data &&
                                orderresponse.data.results &&
                                orderresponse.data.results.length > 0) {
                                let update = {
                                    orderstatus: 'Running',
                                    ordername: WipOrderNo,
                                };
                                let updateresponse = yield this.updateOrderStatus(update);
                                if (updateresponse.status === 200) {
                                    log.error(`Record saved successfully in ShopWorx for new order ${JSON.stringify(updateresponse.data)}`);
                                    res.status(200);
                                    res.json({
                                        msg: `Record saved successfully in ShopWorx for new order`,
                                    });
                                }
                                else {
                                    log.error(`Error in writing new order data in ShopWorx ${JSON.stringify(updateresponse.data)}`);
                                    Utility.checkSessionExpired(updateresponse.data);
                                    res.status(400);
                                    res.json({
                                        msg: `Error in writing new order data in ShopWorx`,
                                    });
                                }
                            }
                            else {
                                let roadmapquery = ``;
                                const roadmapresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.roadmaplist || 'roadmaplist', roadmapquery);
                               debugger; if (roadmapresponse.status == 200) {
                                     let roadmapid = '';
                                    let roadmapname = '';
                                    let roadmaptype = '';
                                    if (roadmapresponse.data &&
                                        roadmapresponse.data.results &&
                                        roadmapresponse.data.results.length > 0) {
                                        roadmapid = roadmapresponse.data.results[0].id;
                                        roadmapname = roadmapresponse.data.results[0].name;
                                        roadmaptype = roadmapresponse.data.results[0].roadmaptype;
                                    }
                                    let producttypequery = `query=productname=="${RecipeName}"`;
                                    const producttyperesponse = yield element_service_1.default.getElementRecords('part', producttypequery);
                                    debugger;if (producttyperesponse.status == 200) {
                                        if (producttyperesponse.data &&
                                            producttyperesponse.data.results &&
                                            producttyperesponse.data.results.length > 0) {
                                            const bomid = producttyperesponse.data.results[0].bomid;
                                            const productnumber = producttyperesponse.data.results[0].productnumber;
                                            const productname = RecipeName;
                                            let payload = {
                                                assetid: config_json_1.default.assetid || 4,
                                                lineid: 1,
                                                linename: ProductionLineNo,
                                                ordercreatedtime: moment_1.default().valueOf(),
                                                ordername: WipOrderNo,
                                                ordernumber: WipOrderNo,
                                                orderstatus: 'New',
                                                ordertype: Number(WipOrderType || 1) == 1 ? 'Work' : 'Rework',
                                                productid: productnumber,
                                                productname: productname,
                                                roadmapid: roadmapid,
                                                roadmapname: roadmapname,
                                                roadmaptype: roadmaptype,
                                                scheduledstart: moment_1.default().valueOf(),
                                                targetcount: Quantity,
                                                reworktargetstation: ReworkStartEquipment || "",
                                                mesrecipe: ProductNo,
                                                source: 2,
                                                bomid: bomid,
                                            };
                                            const response = yield element_service_1.default.createElementRecord(config_json_1.default.elements.order || 'order', payload);
                                           debugger; if (response.status === 200) {
                                                log.error(`Record saved successfully in ShopWorx for new order ${JSON.stringify(response.data)}`);
                                                let orderquery = `query=ordername=="${WipOrderNo}"`;
                                                orderquery += `&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`;
                                                const orderresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.order || 'order', orderquery);
                                                if (orderresponse.status == 200) {
                                                    let order = orderresponse.data.results[0];
                                                    let ordernumber = order.ordernumber;
                                                    const substationresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.substation || 'substation', '');
                                                   debugger; if (substationresponse.status == 200) {
                                                        if (substationresponse.data &&
                                                            substationresponse.data.results &&
                                                            substationresponse.data.results.length > 0) {
                                                            let orderproductpromises = substationresponse.data.results.map((item) => {
                                                                return new Promise((resolve) => {
                                                                    let payload = {
                                                                        lineid: item.lineid,
                                                                        orderid: ordernumber,
                                                                        ordername: WipOrderNo,
                                                                        productid: productnumber,
                                                                        sublineid: item.sublineid,
                                                                        substationid: item.id,
                                                                    };
                                                                    this.writeorderproduct(payload, resolve);
                                                                });
                                                            });
                                                            let orderroadmappromises = substationresponse.data.results.map((item) => {
                                                                return new Promise((resolve) => {
                                                                    this.writeorderroadmap(ordernumber, roadmapid, item.id, resolve);
                                                                });
                                                            });
                                                            Promise.all([
                                                                ...orderproductpromises,
                                                                ...orderroadmappromises,
                                                            ]).then(() => tslib_1.__awaiter(this, void 0, void 0, function* () {
                                                                let startorder = {
                                                                    ordername: WipOrderNo,
                                                                    orderstatus: 'Running',
                                                                };
                                                                const updateorderresponse = yield this.updateOrderStatus(startorder);
                                                                if (updateorderresponse.status == 200) {
                                                                    res.status(200);
                                                                    res.json({
                                                                        msg: `Record saved successfully in ShopWorx for new order`,
                                                                    });
                                                                }
                                                                else {
                                                                    Utility.checkSessionExpired(updateorderresponse.data);
                                                                    res.status(400);
                                                                    res.json({
                                                                        msg: `Error in writing new order data in ShopWorx`,
                                                                    });
                                                                    yield this.updateOrderStatus(startorder);
                                                                }
                                                            }));
                                                        }
                                                    }
                                                    else {
                                                        Utility.checkSessionExpired(substationresponse.data);
                                                        res.status(400);
                                                        res.json({
                                                            msg: `Error in writing new order data in ShopWorx`,
                                                        });
                                                    }
                                                }
                                                else {
                                                    Utility.checkSessionExpired(orderresponse.data);
                                                    res.status(400);
                                                    res.json({
                                                        msg: `Error in writing new order data in ShopWorx`,
                                                    });
                                                }
                                            }
                                            else {
                                                log.error(`Error in writing new order data in ShopWorx ${JSON.stringify(response.data)}`);
                                                Utility.checkSessionExpired(response.data);
                                                res.status(400);
                                                res.json({
                                                    msg: `Error in writing new order data in ShopWorx`,
                                                });
                                            }
                                        }
                                        else {
		                            res.status(400);
		                            res.json({
		                                msg: `Error in writing new order data in ShopWorx`,
		                            });
                                        }
                                    }
                                    else {
 					Utility.checkSessionExpired(producttyperesponse.data);
                                    	res.status(400);
		                            res.json({
		                                msg: `Error in writing new order data in ShopWorx`,
		                            });
                                    }
                                    
                                }
                                else {
                                    Utility.checkSessionExpired(roadmapresponse.data);
                                    res.status(400);
                                    res.json({
                                        msg: `Error in writing new order data in ShopWorx`,
                                    });
                                }
                            }
                        }
                        else {
                            Utility.checkSessionExpired(orderresponse.data);
                            res.status(400);
                            res.json({
                                msg: `Error in writing new order data in ShopWorx`,
                            });
                        }
                    }
                    else {
                        Utility.checkSessionExpired(completedresponse.data);
                        res.status(400);
                        res.json({
                            msg: `Error in writing new order data in ShopWorx `,
                        });
                    }
                }
                else {
                    let update = {
                        orderstatus: 'Interrupted',
                        ordername: WipOrderNo,
                    };
                    let updateresponse = yield this.updateOrderStatus(update);
                    if (updateresponse.status === 200) {
                        log.error(`Record saved successfully in ShopWorx for new order ${JSON.stringify(updateresponse.data)}`);
                        res.status(200);
                        res.json({
                            msg: `Record saved successfully in ShopWorx for new order`,
                        });
                    }
                    else {
                        log.error(`Error in writing new order data in ShopWorx ${JSON.stringify(updateresponse.data)}`);
                        Utility.checkSessionExpired(updateresponse.data);
                        res.status(400);
                        res.json({
                            msg: `Error in writing new order data in ShopWorx`,
                        });
                    }
                }
            }
            catch (error) {
                next(error);
            }
        });
    }
    commitComponentLifeAlarm(req, res, next) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { ProductionLineNo, Equipment, Reasoncode, LifeType, CountValue, } = req.body;
            const elementName = config_json_1.default.elements.componentlifealarm || 'componentlifealarm';
            try {
                let substationquery = `query=name=="${Equipment}"`;
                const substationresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.substation || 'substation', substationquery);
                if (substationresponse.status === 200) {
                    let substationid = '';
                    if (substationresponse.data &&
                        substationresponse.data.results &&
                        substationresponse.data.results.length > 0) {
                        substationid = substationresponse.data.results[0].id;
                    }
                    let payload = {
                        assetid: 4,
                        lineid: 1,
                        linename: 'Line 1',
                        substationname: Equipment,
                        substationid: substationid,
                        reasoncode: Reasoncode,
                        lifetype: LifeType || '',
                        count: CountValue || 0,
                    };
                    const response = yield element_service_1.default.createElementRecord(elementName, payload);
                    if (response.status === 200) {
                        log.error(`Record saved successfully in ShopWorx for componentlifealarm ${JSON.stringify(response.data)}`);
                        res.status(200);
                        res.json({
                            msg: `Record saved successfully in ShopWorx for componentlifealarm ${JSON.stringify(response.data)}`,
                        });
                    }
                    else {
                        log.error(`Error in writing componentlifealarm data in ShopWorx ${JSON.stringify(response.data)}`);
                        Utility.checkSessionExpired(response.data);
                        res.status(400);
                        res.json({
                            msg: `Error in writing componentlifealarm data in ShopWorx`,
                        });
                    }
                }
                else {
                    log.error(`Error in writing componentlifealarm data in ShopWorx ${JSON.stringify(substationresponse.data)}`);
                    Utility.checkSessionExpired(substationresponse.data);
                    res.status(400);
                    res.json({
                        msg: `Error in writing componentlifealarm data in ShopWorx`,
                    });
                }
            }
            catch (error) {
                next(error);
            }
        });
    }
    updateOrderStatus(payload) {
        const elementName = config_json_1.default.elements.order || 'order';
        payload.assetid = config_json_1.default.assetid || 4;
        let query = '';
        if (payload.orderstatus === 'Completed') {
            query = `query=orderstatus=="Running"`;
        }
        else {
            query = `query=ordername=="${payload.ordername}"`;
        }
        query += `&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`;
        return element_service_1.default.updateElementRecordsByQuery(elementName, payload, query);
    }
    writeorderproduct(payload, resolve) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const elementName = 'orderproduct';
            payload.assetid = config_json_1.default.assetid || 4;
            const response = yield element_service_1.default.createElementRecord(elementName, payload);
            if (response.status === 200) {
                log.error(`Record saved successfully in ShopWorx for orderroadmap ${JSON.stringify(response.data)}`);
                if (resolve)
                    resolve();
            }
            else {
                if (resolve)
                    resolve();
                log.error(`Error in writing orderroadmap data in ShopWorx ${JSON.stringify(response.data)}`);
                Utility.checkSessionExpired(response.data);
                this.writeorderproduct(payload);
            }
        });
    }
    writeorderroadmap(ordernumber, roadmapid, substationid, resolve) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            let roadmapdetailquery = `query=roadmapid=="${roadmapid}"`;
            roadmapdetailquery += `%26%26substationid=="${substationid}"`;
            const roadmapdetailresponse = yield element_service_1.default.getElementRecords(config_json_1.default.elements.roadmapdetails || 'roadmapdetail', roadmapdetailquery);
            if (roadmapdetailresponse.status == 200) {
                if (roadmapdetailresponse.data &&
                    roadmapdetailresponse.data.results &&
                    roadmapdetailresponse.data.results.length > 0) {
                    const roadmapdetail = roadmapdetailresponse.data.results[0];
                    const elementName = 'orderroadmap';
                    let payload = {
                        amtpresubstation: roadmapdetail.amtpresubstation,
                        assetid: config_json_1.default.assetid || 4,
                        lineid: 1,
                        orderid: ordernumber,
                        prestationid: roadmapdetail.prestationid,
                        prestationname: roadmapdetail.prestationname,
                        presubstationid: roadmapdetail.presubstationid,
                        presubstationname: roadmapdetail.presubstationname,
                        roadmapid: roadmapdetail.roadmapid,
                        sublineid: roadmapdetail.sublineid,
                        sublinename: roadmapdetail.sublinename,
                        substationid: roadmapdetail.substationid,
                        substationname: roadmapdetail.substationname,
                    };
                    const response = yield element_service_1.default.createElementRecord(elementName, payload);
                    if (response.status === 200) {
                        log.error(`Record saved successfully in ShopWorx for orderroadmap ${JSON.stringify(response.data)}`);
                        if (resolve)
                            resolve();
                    }
                    else {
                        if (resolve)
                            resolve();
                        log.error(`Error in writing orderroadmap data in ShopWorx ${JSON.stringify(response.data)}`);
                        Utility.checkSessionExpired(response.data);
                        this.writeorderroadmap(ordernumber, roadmapid, substationid);
                    }
                }
                else {
                    resolve();
                }
            }
            else {
                log.error(`Error in writing orderroadmap data in ShopWorx ${JSON.stringify(roadmapdetailresponse.data)}`);
                Utility.checkSessionExpired(roadmapdetailresponse.data);
                this.writeorderroadmap(ordernumber, roadmapid, substationid, resolve);
            }
        });
    }
}
exports.default = new Controller();
