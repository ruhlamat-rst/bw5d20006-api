'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerTimeJS = void 0;
const tslib_1 = require("tslib");
const servertime_service_1 = tslib_1.__importDefault(require("../service/servertime.service"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const Utility = tslib_1.__importStar(require("./utility"));
function servertime(config = config_json_1.default, utility = Utility) {
    const log = bunyan_1.default.createLogger({
        name: 'ServerTime',
        level: config.logger.loglevel,
    });
    let retryServerTimer = 10;
    return {
        getServerTime() {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                try {
                    const response = yield servertime_service_1.default.getServerTime({});
                    if (response.data && response.data.results) {
                    }
                    else {
                        utility.checkSessionExpired(response.data);
                    }
                }
                catch (ex) {
                    log.error(`Exception to fetch servertime ${ex}`);
                }
                yield utility.setTimer(retryServerTimer);
                this.getServerTime();
            });
        },
    };
}
exports.ServerTimeJS = {
    servertime,
};
